---
title: "CSE-160 Final Project Models"
output: html_notebook
author: Asher Hamrick
date: 28 March 2021
---

Data Reading
```{r}
# Read dataset
stroke.dataset <- read.csv("./healthcare-dataset-stroke-data.csv", stringsAsFactors = TRUE);
str(stroke.dataset)
summary(stroke.dataset)
```

Data Cleaning
```{r}
# Clean data of outlier values and N/A
stroke.dataset$id <- NULL;

# Other has only 1 data, eliminate it!
stroke.dataset <- stroke.dataset[stroke.dataset$gender != "Other",];

# Never_worked has only 22 data, eliminate it!
stroke.dataset <- stroke.dataset[stroke.dataset$work_type != "Never_worked",];

# Eliminate N/A values
stroke.dataset <- stroke.dataset[stroke.dataset$gender != "N/A" & stroke.dataset$age != "N/A" & stroke.dataset$hypertension != "N/A" & stroke.dataset$heart_disease != "N/A" & stroke.dataset$ever_married != "N/A" & stroke.dataset$work_type != "N/A" & stroke.dataset$Residence_type != "N/A" & stroke.dataset$avg_glucose_level != "N/A" & stroke.dataset$bmi != "N/A" & stroke.dataset$smoking_status != "N/A",];

# bmi was read as factors initially, it should be numeric
stroke.dataset$bmi <- as.numeric(stroke.dataset$bmi)

# stroke was defaulted to numeric since it is 0 and 1, but it's actually a factor
stroke.dataset$stroke <- as.factor(stroke.dataset$stroke)

str(stroke.dataset)
summary(stroke.dataset)
```

Split training and testing data
```{r}
# Sample data for training and testing datasets
sample.size <- length(stroke.dataset$stroke);
# 25% training data 75% testing data
train.size <- round(sample.size * (3/4));
sample <- sample(1:sample.size, size = train.size, replace = FALSE, prob = rep(1 / sample.size, sample.size));

# fill in the training and testing data
train.data <- stroke.dataset[sample,];
test.data <- stroke.dataset[-sample,];
# Perform 10-fold cross validation
foldsize <- round(length(train.data[,1]) / 10);
```

Decision Tree
```{r}
library(rpart);
d.t.train <- train.data;
length.no.stroke <- length(d.t.train[d.t.train$stroke != 1, 1]);
stroke.supersample <- d.t.train[d.t.train$stroke != 0,];
iterations <- round(length.no.stroke / length(stroke.supersample[,1]));
count <- 0;
# super sample the data so that the amout of stroke case equal to the amount of non stroke case
while (count < iterations) {
  d.t.train <- rbind(d.t.train, stroke.supersample);
  count <- count + 1;
}
# Create classifier
decision.tree.model <- rpart(stroke ~ ., data = d.t.train, method="class");
decision.tree.model.test <- predict(decision.tree.model, newdata = test.data)[,2];
```

Logistic regression
```{r}
# Assemble logistic regression model

logistic.regression.model <- glm(stroke ~ ., data = d.t.train, family = "binomial");
logistic.regression.model.test <- predict(logistic.regression.model, newdata = test.data, type = "response");

```

K Nearest neighbour
This is a classifier model used to compare to the probabilistic models we used. 
```{r}
library(kknn);
# Build k means model
k.means.model <- kknn(stroke ~ ., train = train.data, test = test.data);
# Fit results from test data
fitted.data <- fitted(k.means.model);
```

Naive Bayes
```{r}
library(e1071);
# Build Naive Bayes classifier model
# Loop through the 10 folds
index <- 1;
naive.bayes.test <- c();
while (index <= 10) {
  # Calculate indices to exclude
  begin_index <- (foldsize * index) - foldsize + 1;
  end_index <- foldsize * index;
  # Create classifier
  naive.bayes.model <- naiveBayes(train.data[-(begin_index:end_index),1:10], train.data[-(begin_index:end_index),11]);
  naive.bayes.test <- c(naive.bayes.test, predict(naive.bayes.model, test.data, type = c("raw"))[,2]);
  index <- index + 1;
}

# Test Naive Bayes classifier

```


ROC curve
```{r}
library(ROCR);
# Build ROCR Curves
length <- length(test.data[,1]);
index <- 1;

# get the average of the naive bayes classifier so that it will work on the ROC curve
while (index <= length) {
  if (is.na(naive.bayes.test[index + (9 * length)])) {
    i <- 0;
    while (i < 9) {
      naive.bayes.test[index] <- naive.bayes.test[index] + naive.bayes.test[index + (i * length)];
      i <- i + 1;
    }
    naive.bayes.test[index] <- naive.bayes.test[index] / 9;
  }
  else {
    i <- 0;
    while (i < 10) {
      naive.bayes.test[index] <- naive.bayes.test[index] + naive.bayes.test[index + (i * length)];
      i <- i + 1;
    }
    naive.bayes.test[index] <- naive.bayes.test[index] / 10;
  }
  index <- index + 1;
}
naive.bayes.test <- naive.bayes.test[1:length];

# fit the rest of the results to ROC curve
logistic.ROCRpred <- prediction(logistic.regression.model.test, test.data[,11]);
logistic.ROCRperf <- performance(logistic.ROCRpred, 'tpr', 'fpr');
decision.tree.ROCRpred <- prediction(decision.tree.model.test, test.data[,11]);
decision.tree.ROCRperf <- performance(decision.tree.ROCRpred, 'tpr', 'fpr');
naive.bayes.ROCRpred <- prediction(naive.bayes.test, test.data[,11]);
naive.bayes.ROCRperf <- performance(naive.bayes.ROCRpred, 'tpr', 'fpr');

# plot the ROC curve
plot(naive.bayes.ROCRperf, col="blue", text.adj=c(-0.2,1), main="Classifier ROC Curve Comparison"); 
plot(logistic.ROCRperf, add=TRUE, col="red");
plot(decision.tree.ROCRperf, add=TRUE, col="green");

```


Turns all the probabilities into actual predictions, for recall, precision and accuracy
```{r}
index <- 1;
l.r.prediction <- c();
d.t.prediction <- c();
n.b.prediction <- c();

while (index <= length) {
  if (logistic.regression.model.test[index] > 0.5) {
    l.r.prediction <- c(l.r.prediction, 1);
  }
  else {
    l.r.prediction <- c(l.r.prediction, 0);
  }
  if (naive.bayes.test[index] > 0.5) {
    n.b.prediction <- c(n.b.prediction, 1);
  }
  else {
    n.b.prediction <- c(n.b.prediction, 0);
  }
  if (decision.tree.model.test[index] > 0.5) {
    d.t.prediction <- c(d.t.prediction, 1);
  }
  else {
    d.t.prediction <- c(d.t.prediction, 0);
  }
  index <- index + 1;
}
```


Create the confusion matricies for every models
```{r}
l.r.matrix <- table(data.frame(true=test.data$stroke, pred=l.r.prediction));
d.t.matrix <- table(data.frame(true=test.data$stroke, pred=d.t.prediction));
n.b.matrix <- table(data.frame(true=test.data$stroke, pred=n.b.prediction));
k.k.matrix <- table(data.frame(true=test.data$stroke, pred=fitted.data));

cat("Logistic Regression Model:\n");
l.r.matrix;
cat("\nDecision Tree Model:\n");
d.t.matrix;
cat("\nNaive Bayes Classifier Model:\n");
n.b.matrix;
cat("\nK-Nearest Neighbor Model:\n");
k.k.matrix;
```

Calculate recall, precision, accuracy
```{r}
  l.r.precision <- l.r.matrix[1] / (l.r.matrix[1] + l.r.matrix[3]);
  l.r.recall <- l.r.matrix[1] / (l.r.matrix[1] + l.r.matrix[2]);
  l.r.accuracy <- (l.r.matrix[1] + l.r.matrix[4]) / sum(l.r.matrix);
  d.t.precision <- d.t.matrix[1] / (d.t.matrix[1] + d.t.matrix[3]);
  d.t.recall <- d.t.matrix[1] / (d.t.matrix[1] + d.t.matrix[2]);
  d.t.accuracy <- (d.t.matrix[1] + d.t.matrix[4]) / sum(d.t.matrix);
  n.b.precision <- n.b.matrix[1] / (n.b.matrix[1] + n.b.matrix[3]);
  n.b.recall <- n.b.matrix[1] / (n.b.matrix[1] + n.b.matrix[2]);
  n.b.accuracy <- (n.b.matrix[1] + n.b.matrix[4]) / sum(n.b.matrix);
  k.k.precision <- k.k.matrix[1] / (k.k.matrix[1] + k.k.matrix[3]);
  k.k.recall <- k.k.matrix[1] / (k.k.matrix[1] + k.k.matrix[2]);
  k.k.accuracy <- (k.k.matrix[1] + k.k.matrix[4]) / sum(k.k.matrix);
```

display everything
```{r}
rows <- c("logistic regression", "decision tree", "naive bayes", "k nearest neighbor");
precision <- c(l.r.precision, d.t.precision, n.b.precision, k.k.precision);
recall <- c(l.r.recall, d.t.recall, n.b.recall, k.k.recall);
accuracy <- c(l.r.accuracy, d.t.accuracy, n.b.accuracy, k.k.accuracy);
performance.data <- data.frame(precision, recall, accuracy, row.names = rows);
performance.data
```

The recall is not significantly worse than the others
Meanwhile, precision and accuracy is significantly higher


```{r}
plot(decision.tree.model.test)
plot(logistic.regression.model.test)
plot(naive.bayes.test)

```
Naive Bayes scews toward 0, which represents the actual stroke situation.

Conclusion:  Naive baysw is the best



